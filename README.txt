
WARNING
-------
I'm serious, you shouldn't be using this module. Usage really just hurts the
community at large. Follow my logic here.

You install this module on your client sites.
You have a falling out with a client and money is owed or some other situation.
The client revokes shell/ftp, etc, access to their servers.
You go ahead and throw the killswitch.
The client then blames Drupal for the problems created.
The client begins to bad mouth Drupal to their colleagues.

TL;DR Don't use it because it will hurt you in the long run.

INSTALLATION
------------
This module has a dependency upon bad_judgement module for a good reason.
Untar the archived module into sites/*/modules directory.
Edit the killswitch.module file with the paths of the kill and the restore.
define('KILLSWITCH_KILL_PATH', 'some/path/here');
define('KILLSWITCH_LIVE_PATH', 'some/other/path/here');

Don't even look at line 10. See USAGE.

UNINSTALLATION
--------------
There are several options for disabling this module.
1. Delete the killswitch directory.
2. Use drush: drush dis killswitch
3. Disable the module from mysql: UPDATE system SET status = '0' WHERE name = 'killswitch';

USAGE
-----
Don't. See WARNING above.

DISCLAIMER
----------
If you fail to heed my warnings, see WARNING and USAGE, and use this module,
the module author cannot be held accountable for your actions. Please obtain
legal advice if you are unsure the ramifications of using this module.