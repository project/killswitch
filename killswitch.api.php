<?php

/**
 * @file
 * Hooks provided by the Killswitch module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Hide a module.
 *
 * This hook will hide the returned module(s) from the module administration
 * page, admin/modules.
 *
 * This hook is invoked by killswitch_form_system_modules_alter(), which alters
 * the form returned by system_modules().
 *
 * @see killswitch_form_system_modules_alter()
 * @see system_modules()
 *
 * @return
 *   An array of Strings, interpreted as modules to be hidden.
 *
 * See killswitch_killswitch_hide() for an example that hides only one module.
 * This example hides all modules.
 */
function hook_killswitch_hide() {
  return module_list();
}

/**
 * @} End of "addtogroup hooks".
 */
